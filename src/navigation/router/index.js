import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Main, Splash, Test} from '../../screens';
const Stack = createNativeStackNavigator();
const Hide = {headerShown: false};

const index = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={Splash} options={Hide} />
        <Stack.Screen name="Main" component={Main} options={Hide} />
        <Stack.Screen name="Test" component={Test} options={Hide} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default index;
