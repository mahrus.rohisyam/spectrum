import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import {Logo, responsiveHeight, responsiveWidth} from '../../assets';

export default class index extends Component {
  componentDidMount() {
    const {navigation} = this.props;
    setTimeout(() => {
      navigation.replace('Main');
    }, 2000);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={[styles.logo]}>
          <Image resizeMode="cover" style={styles.logo} source={Logo} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    flex: 1,
  },
  logo: {
    width: responsiveWidth(150),
    height: responsiveHeight(230),
  },
});
