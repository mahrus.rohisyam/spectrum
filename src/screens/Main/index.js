import React, {Component} from 'react';
import { ActivityIndicator, BackHandler, StyleSheet, Text, View} from 'react-native';
import WebView from 'react-native-webview';

export default class WebViewMoviezSpace extends Component {
  constructor(props) {
    super(props);
    this.WEBVIEW_REF = React.createRef();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    this.WEBVIEW_REF.current.goBack();
    return true;
  };

  onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack,
    });
  }

  render() {
    const renderLoadingView = () =>{
      setTimeout(()=>{
        return (
          <View style={styles.loadingView}>
            {/* <ActivityIndicator size="large" color="blue" /> */}
            <Text>Test</Text>
          </View>
        );
      }, 1000)
    }
    return (
      <WebView
        source={{uri: 'https://spectrum.trensains.sch.id'}}
        ref={this.WEBVIEW_REF}
        originWhitelist={'*'}
        startInLoadingState={true}
        renderLoading={renderLoadingView}
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        mixedContentMode="always"
        allowUniversalAccessFromFileURLs={true}
        pullToRefreshEnabled={true}
      />
    );
  }
}

const styles = StyleSheet.create({
  renderLoadingView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
});